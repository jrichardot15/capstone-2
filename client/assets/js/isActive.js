let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let isActive = document.querySelector("#isActiveTrue")
let token = localStorage.getItem("token")

    isActive.addEventListener("click", () => {

    fetch(`http://localhost:3000/api/courses/isActiveTrue/${courseId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },      
        body: JSON.stringify({

        })
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        if(data === true){
            //if creation of new course is successful, redirect to courses page
            window.location.replace('./courses.html')
        }else{
            alert('Something went wrong')
        }
    })
})