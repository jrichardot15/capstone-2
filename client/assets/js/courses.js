let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
</div>`
}

//fetch the courses from our API
if (adminUser == 'false' || !adminUser) {
fetch('http://localhost:3000/api/courses')
.then(res => res.json())
.then(data => {

	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-success text-white btn-block editButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `
				<div class="container">
				<div class="row">
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="courses-edit" class="btn btn-warning text-white  editButton col-6">Edit</a>
 
				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} id="courses-delete" class="btn btn-danger text-white editButton col-6">Delete</a>

				<a href="./enrollees.html?courseId=${course._id}" value=${course._id} id="courses-enrollees" class="btn btn-info text-white editButton col-6">Enrollees</a>

				<a href="./isActive.html?courseId=${course._id}" value=${course._id} id="isActive" class="btn btn-info text-white editButton col-6">Active</a>	
				</div>
				</div>`

			}

			return (
			`<div class="col-md-6 my-3" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
				<div class="card">
					<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-right">₱ ${course.price}</p>
					</div>

				<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})
}else{
	fetch('http://localhost:3000/api/courses/admin')
	.then(res => res.json())
	.then(data => {
		let courseData;

		if (data.length < 1) {
			courseData = "No course available"
		} else {
			courseData = data.map(course => {
				if (!course.isActive) {
					cardFooter = `
				<div class="container">
				<div class="row">
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="courses-edit" class="btn btn-warning text-white  editButton col-6">Edit</a>
 
				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} id="courses-delete" class="btn btn-danger text-white editButton col-6">Delete</a>

				<a href="./enrollees.html?courseId=${course._id}" value=${course._id} id="courses-enrollees" class="btn btn-info text-white editButton col-6">Enrollees</a>

				<a href="./isActive.html?courseId=${course._id}" value=${course._id} id="isActive" class="btn btn-info text-white editButton col-6">Reactivate</a>	
				</div>
				</div>`
				} else {
					cardFooter = `
				<div class="container">
				<div class="row">
				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="courses-edit" class="btn btn-warning text-white  editButton col-6">Edit</a>
 
				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} id="courses-delete" class="btn btn-danger text-white editButton col-6">Delete</a>

				<a href="./enrollees.html?courseId=${course._id}" value=${course._id} id="courses-enrollees" class="btn btn-info text-white editButton col-12">Enrollees</a>

				</div>
				</div>`
				}
				
				if (!course.isActive) {
					return (`<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-right">₱ ${course.price}</p>
									<p class="card-text text-left">Status: Inactive
								</div>

								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
							</div>`
						)
				} else {
					return (`<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-right">₱ ${course.price}</p>
									<p class="card-text text-left">Status: Active
								</div>

								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>`
					)
				}
				
			}).join("")
		}

		let container = document.querySelector('#coursesContainer')

		if (container == null) {
			return (
				container.innerHTML =
					`
						<h5>No courses available</h5>
					`
				)
		} else {
			container.innerHTML = courseData
		}
	})
}
// if(adminUser === true){
// 	fetch('http://localhost:3000/api/courses')
// 	.then(res => {
// 		return res.json()
// 	})
// 	.then(data => {
		
// 	})
// }else{
// 	return window.location.replace("./courses.html")

// }