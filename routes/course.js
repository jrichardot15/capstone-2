const express = require('express')
const router = express.Router()
const auth = require("../auth")
const UserController = require('../controllers/user')
const CourseController = require('../controllers/course')

//get all courses
router.get('/', (req, res) => {
	CourseController.getAllActive().then(resultsFromFind => res.send(resultsFromFind))
})

//if user is admin
router.get('/admin', (req, res) => {
	CourseController.getAllCourses().then(resultFromFindAdmin => res.send(resultFromFindAdmin))
})

//get a single course
router.get('/:courseId', (req, res) => {
	let courseId = req.params.courseId
	CourseController.get({courseId}).then(resultFromFindById => res.send(resultFromFindById))
})

//create a new course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(resultFromAdd => res.send(resultFromAdd))
})

//update a course
router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})

//archive (delete) a course
router.delete('/:courseId', auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.archive({courseId}).then(resultFromArchive => res.send(resultFromArchive))
})

//isActive: true
router.put('/isActiveTrue/:courseId', auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.isActiveTrue({courseId}).then(resultFromIsActiveTrue => res.send(resultFromIsActiveTrue))
})


module.exports = router